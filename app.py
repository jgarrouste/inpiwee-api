"""Simple pylint Demo"""
from flask import Flask, jsonify
APP = Flask(__name__)

MESSAGES = [
    {
        'id': 1,
        'message': u'Hello',
    },
    {
        'id': 2,
        'message': u' You!',
    },
]

@APP.route('/', methods=['GET'])
def get_tasks():
    """Return messages"""
    return jsonify({'messages': MESSAGES})

if __name__ == '__main__':
    APP.run(host='0.0.0.0')
    